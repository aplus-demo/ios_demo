//
//  ViewController.m
//  APJDemo
//
//  Created by Andy on 2023/4/20.
//

#import "ViewController.h"
#import "SetLanguageController.h"
#import <APJSDK/APJSDK.h>
#import <Singular/Singular.h>

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>

@interface ViewController ()<APJSDKCallback>
@property(nonatomic, strong)UILabel* msgLb;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColor.whiteColor;
    self.navigationController.navigationBar.hidden = YES;
    NSArray* titleAry = @[@"初始化",@"登录",@"设置语言",@"账号中心",
                          @"查询商品信息",@"购买Item1",@"购买Item2",
                          @"FB单个网页链接", @"FB单个图片链接",
                          @"FB单个本地路径", @"FB多个图片链接",
                          @"FB多个本地路径", @"原生图片链接",
                          @"原生图片路径", @"事件埋点",
                          @"同步用户信息", @"打开APP评分弹框",@"打开客服页面"];
    
    for(int i=0; i<titleAry.count; i++){
        [self addBtWithIndex:i title:titleAry[i]];
    }
    
    _msgLb = [[UILabel alloc] init];
    _msgLb.backgroundColor = UIColor.lightGrayColor;
    _msgLb.frame = CGRectMake(40, self.view.frame.size.height-70,
                              self.view.frame.size.width-100, 50);
    [self.view addSubview:_msgLb];
}

-(void)btTaped:(UIButton*)bt{
    NSInteger index = bt.tag;
    
    if(index == 0){
        [APJSDK initSDK:EN sdkCallbackDelegate:self];
        
    }else if(index == 1){
        [APJSDK startLogin];
        
    }else if(index == 2){
//        [self presentViewController:[[SetLanguageController alloc]init] animated:NO completion:nil];
        
        //测试谷歌广告
        [APJSDK showGoogleInterstitialAd:@"ca-app-pub-3940256099942544/5224354917"];
        
    }else if(index == 3){
        [APJSDK openAccountCenter];
        
    }else if(index == 4){
        NSMutableArray* ary = [[NSMutableArray alloc] init];
        [ary addObject:@"com.aplusjapan.sdkdevelop.item1"];
        [ary addObject:@"com.aplusjapan.sdkdevelop.item2"];
        [APJSDK queryProductInfo:ary];
        
    }else if(index == 5){
        [APJSDK buyProduct:@"com.aplusjapan.sdkdevelop.item1"
               cpAccountId:@"aa1" 
                cpServerId:@"aa2"
                  cpRoleId:@"aa3" 
                 cpOrderId:[[NSUUID UUID] UUIDString] //游戏的订单ID
               cpNotifyUrl:@"aa5"  //游戏提供给我们游戏服务端接收购买通知的url
                cpRefundUrl:@""
                 cpExtInfo:@"aa6"];
        
        
    }else if(index == 6){
        [APJSDK buyProduct:@"com.aplusjapan.sdkdevelop.item2"
               cpAccountId:@"bb1" 
                cpServerId:@"bb2"
                  cpRoleId:@"bb3" 
                 cpOrderId:[[NSUUID UUID] UUIDString] //游戏的订单ID
               cpNotifyUrl:@"bb5"  //游戏提供给我们游戏服务端接收购买通知的url
               cpRefundUrl:@""
                 cpExtInfo:@"bb6"];
        
    }else if(index == 7){
        [APJSDK shareFacebookPathOrUrl:@"https://www.baidu.com"];
        
    }else if(index == 8){
        [APJSDK shareFacebookPathOrUrl:@"https://static.zhihu.com/heifetz/assets/guide-cover-5.294257c3.jpg"];
        
    }else if(index == 9){
        [APJSDK shareFacebookPathOrUrl:[self localImagePath]];
        
    }else if(index == 10){
        NSMutableArray* ary = [[NSMutableArray alloc] init];
        [ary addObject:@"https://static.zhihu.com/heifetz/assets/guide-cover-5.294257c3.jpg"];
        [ary addObject:@"https://static.zhihu.com/heifetz/assets/guide-cover-5.294257c3.jpg"];
        [APJSDK shareFacebookImageList:ary];
        
    }else if(index == 11){
        NSMutableArray* ary = [[NSMutableArray alloc] init];
        [ary addObject:[self localImagePath]];
        [APJSDK shareFacebookImageList:ary];
       
        
    }else if(index == 12){
        NSMutableArray* ary = [[NSMutableArray alloc] init];
        [ary addObject:@"https://static.zhihu.com/heifetz/assets/guide-cover-5.294257c3.jpg"];
        [ary addObject:@"https://static.zhihu.com/heifetz/assets/guide-cover-5.294257c3.jpg"];
        [APJSDK shareNativeList:ary];
        
    }else if(index == 13){
        NSMutableArray* ary = [[NSMutableArray alloc] init];
        [ary addObject:[self localImagePath]];
        [ary addObject:[self localImagePath]];
        [APJSDK shareNativeList:ary];
        
    }else if(index == 14){
      
       
        
    }else if(index == 15){
        //都是字符，不能空！
        [APJSDK syncRoleInfo:@"account_id_ios"
                   gServerId:@"server_id_ios"
                 gServerName:@"server_name_ios" 
                     gRoleId:@"role_id_ios"
                   gRoleName:@"role_name_ios" 
                      gLevel:@"level_1_ios"];
       
        
    }else if(index == 16){
        [APJSDK requestReview];
       
    }else if(index == 17){
        [APJSDK openAIHelp:@"Welcome~"];
       
    }
}

-(void)addBtWithIndex:(int)i title:(NSString*)title{
    int tmp = UIScreen.mainScreen.bounds.size.width / 130;
    UIButton* bt = [UIButton buttonWithType:UIButtonTypeSystem];
    [bt setTitle:title forState:UIControlStateNormal];
    [bt setTitle:title forState:UIControlStateHighlighted];
    bt.tag = i;
    bt.frame = CGRectMake(50 + (i%tmp)*130, 20 + (i/tmp)*50, 120, 40);
    [bt setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    [bt setTitleColor:UIColor.whiteColor forState:UIControlStateHighlighted];
    bt.backgroundColor = UIColor.lightGrayColor;
    [bt addTarget:self action:@selector(btTaped:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:bt];
}


-(NSString*)localImagePath{
    UIImage* image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"yqcr" ofType:@"png"]];
    NSArray* dir = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString* imgPath = [[dir objectAtIndex:0]stringByAppendingPathComponent:[NSString stringWithFormat:@"yqcr.png"]];
    
    if(![[NSFileManager defaultManager] fileExistsAtPath:imgPath]){
        BOOL result = [UIImagePNGRepresentation(image)writeToFile:imgPath  atomically:YES];
            if (result == YES) {
                NSLog(@"图片保存成功");
            }else{
              NSLog(@"图片保存失败");
        }
    }
    return imgPath;
}

-(void)processResult:(NSString*)str{
    NSLog(@"\n--- 打印 ---\n%@\n\n", str);
    _msgLb.text = str;
}

//MARK:  SDK回调
- (void)onApjSDKInitSuccess {
    [self processResult: @"初始化成功"];
}

- (void)onApjSDKInitFail:(int)code msg:(nonnull NSString *)msg {
    [self processResult: msg];
}

- (void)onApjSDKLoginSuccess:(nonnull APJUserModel *)userModel {
    [self processResult: [APJUserModel userId]];
}

- (void)onApjSDKLoginFail:(int)code msg:(nonnull NSString *)msg {
    [self processResult: msg];
}

- (void)onApjSDKLogoutSuccess {
    [self processResult: @"退出登录"];
}

- (void)onApjSDKAccountDeleteSuccess {
    [self processResult:@"删除成功"];
}

-(void) onApjBindAPJAccountSuccess:(NSString*)email{
    [self processResult:email];
}

-(void) onApjSDKPaymentSuccess:(NSString*) productId
                    apjOrderId:(NSString*) apjOrderId
                     cpOrderId:(NSString*) cpOrderId{       //购买成功
    [self processResult:[NSString stringWithFormat:@"购买成功：%@ - %@ - %@",productId,apjOrderId,cpOrderId]];
}

-(void) onApjSDKPaymentFail:(NSInteger) code msg:(NSString*) msg{ //购买失败
    [self processResult:[NSString stringWithFormat:@"购买失败：%ld。 %@", code, msg]];
}

-(void) onApjSDKQueryProductSuccess:(NSArray<APJProductModel*>*) list{ //查询商品信息成功
    APJProductModel* md = [list objectAtIndex:0];
    [self processResult:[NSString stringWithFormat:@"查询成功: %@ %@", md.productId, md.name]];
}

-(void) onApjSDKQueryProductFail:(int) code msg:(NSString*) msg{ //查询商品信息失败
    [self processResult:@"查询失败"];
}

-(void) onApjSDKReceiveNotifyToken:(NSString*)token{ //Firebase的推送token
    [self processResult:[NSString stringWithFormat:@"APNS token: %@", token]];
}

-(void) onApjSDKFetchAppsflyerDeepLinkValueSuccess:(NSString*)deepLinkValue{
    [self processResult:[NSString stringWithFormat:@"deepLinkValue: %@", deepLinkValue]];
}

-(void) onApjSDKShareResult:(APJShareResult)shareResult{
    [self processResult:[NSString stringWithFormat:@"分享结果：code = %d", (int)shareResult]];
}

-(void) onApjReceiveIDFAString:(NSString*)idfaString{
    [self processResult:[NSString stringWithFormat:@"IDFA = %@",idfaString]];
}

-(void) onApjSDKUserType:(int)userType msg:(NSString*)msg{
    [self processResult:msg];
}

- (void)onApjEmailRegisterSuccess:(NSString *)email{
    [self processResult:email];
}

- (void)onApjSDKSyncRoleInfoResult:(Boolean)isSuccess msg:(nonnull NSString *)msg { 
    [self processResult:msg];
}

-(void) onApjSDKGoogleAdClicked:(NSString*) adUnitId{  //谷歌广告：点击广告
    [self processResult:adUnitId];
}

-(void) onApjSDKGoogleAdFinishSuccess:(NSString*) adUnitId{ //谷歌广告：广告关闭
    [self processResult:adUnitId];
}

@end

