//
//  SetLanguageController.m
//  APJDemo
//
//  Created by Andy on 2023/5/2.
//

#import "SetLanguageController.h"
#import <APJSDK/APJSDK.h>

@interface SetLanguageController ()
@property(nonatomic, strong)UIButton* backBt;
@end

@implementation SetLanguageController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColor.whiteColor;
    NSArray* titleAry = @[@"英语",@"德语",@"法语",
                          @"葡萄牙语",@"西班牙语",@"阿拉伯语", @"日语", @"韩语"];
    
    _backBt = [[UIButton alloc] init];
    [_backBt setImage:[UIImage imageNamed:@"back_icon"] forState:UIControlStateNormal];
    [_backBt setImage:[UIImage imageNamed:@"back_icon"] forState:UIControlStateHighlighted];
    [_backBt addTarget:self action:@selector(backTaped) forControlEvents:UIControlEventTouchUpInside];
    _backBt.frame = CGRectMake(30, 30, 30, 30);
    [self.view addSubview:_backBt];
 
    for(int i=0; i<titleAry.count; i++){
        [self addBtWithIndex:i title:titleAry[i]];
    }
}

-(void)backTaped{
    [self dismissViewControllerAnimated:NO completion:nil];
}

-(void)btTaped:(UIButton*)bt{
    NSInteger index = bt.tag;
    if(index == 0){
        [APJSDK setSDKLanguage:EN];
    }else if(index == 1){
        [APJSDK setSDKLanguage:DE];
    }else if(index == 2){
        [APJSDK setSDKLanguage:FR];
    }else if(index == 3){
        [APJSDK setSDKLanguage:PT];
    }else if(index == 4){
        [APJSDK setSDKLanguage:ES];
    }else if(index == 5){
        [APJSDK setSDKLanguage:Ar];
    }else if(index == 6){
        [APJSDK setSDKLanguage:JP];
    }else if(index == 7){
        [APJSDK setSDKLanguage:KR];
    }
    
    [self backTaped];
}

-(void)addBtWithIndex:(int)i title:(NSString*)title{
    int tmp = UIScreen.mainScreen.bounds.size.width / 130;
    UIButton* bt = [UIButton buttonWithType:UIButtonTypeSystem];
    [bt setTitle:title forState:UIControlStateNormal];
    [bt setTitle:title forState:UIControlStateHighlighted];
    bt.tag = i;
    bt.frame = CGRectMake(60 + (i%tmp)*110, 70 + (i/tmp)*50, 100, 40);
    [bt setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    [bt setTitleColor:UIColor.whiteColor forState:UIControlStateHighlighted];
    bt.backgroundColor = UIColor.lightGrayColor;
    [bt addTarget:self action:@selector(btTaped:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:bt];
}

@end
