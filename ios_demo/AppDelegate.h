//
//  AppDelegate.h
//  ios_demo
//
//  Created by Andy on 2023/6/8.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>
@property (strong, nonatomic) UIWindow *window;

@end

