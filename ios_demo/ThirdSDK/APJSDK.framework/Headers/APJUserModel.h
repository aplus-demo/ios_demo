//
//  APJUserModel.h
//  APJSDK
//
//  Created by Andy on 2023/4/21.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef enum : NSUInteger { //与后台对应
    APJAccount = 10,
    AppleAccount = 1,
    GoogleAccount = 2,
    CRAccount = 5,
    GuestAccount = 0,
} APJAccountType;

typedef enum : NSUInteger {
    CloseAll,
    Back,
    None,
} APJCloseType;

typedef enum : NSUInteger {
    AutoLoginSave,
    GuestLoginSave,
    ThirdLoginSave,
    EmailLoginSave,
    EmailRegisterSave,
    EmailResetSave,
    PwdResetSave,
    ThirdBindSave,
    ConfirmDeviceSave,
} APJSaveType;

typedef enum : NSUInteger { //1:Apple 2:Google
    AppleLogin = 1,
    GoogleLogin = 2,
    CRLogin = 5, 
} ThirdLoginType;

typedef enum : NSUInteger {
    Login, //第三方用于登录
    Bind,  //第三方用于绑定
} LoginOrBindType;

typedef enum : NSUInteger {
    AppsflyerEvent,
    SingularEvent,
    SegmentEvent,
    ThinkingDataEvent,
    FirebaseEvent,
} APJEventPlatform;

@interface APJUserModel : NSObject

-(instancetype)initWithDataObj:(NSDictionary*)dataObj;

-(void)saveToLocalAndCallback:(APJSaveType)saveType
                     closeType:(APJCloseType)type
                     withCallback:(BOOL)withCallback;

-(void)processLoginsSuccess:(APJCloseType)type
               withCallback:(BOOL)withCallback;

-(APJAccountType)getAccountType;

-(NSString*)getUserId;

-(NSString*)getAccessToken;

-(NSString*)getUserEmail;

-(NSString*)getCrUserId; //CR账号登录的才有，其他账号登录是空

+(NSString*)userId;

+(NSString*)accessToken;

+(APJUserModel*)currentUser;

+(BOOL)isLogin;

+(void)clearAllLocalUserInfo;

+(Boolean)isAdult;

+(Boolean)isBirthdaySet;

+(NSString*)typeName:(APJAccountType)type;

@end

NS_ASSUME_NONNULL_END
