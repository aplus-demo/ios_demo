//
//  APJSDKCallback.h
//  APJSDK
//
//  Created by Andy on 2023/4/26.
//

#import <Foundation/Foundation.h>
@class APJUserModel;
@class APJProductModel;

NS_ASSUME_NONNULL_BEGIN

typedef enum : NSUInteger {
    ShareSuccess = 0,
    ShareCancel = 1,
    ShareFail = 2,
    NativeShareFinish = 3,
} APJShareResult;

@protocol APJSDKCallback <NSObject>

//account
-(void) onApjSDKInitSuccess; //初始化成功
-(void) onApjEmailRegisterSuccess:(NSString*)email;  //APJ 邮箱账号注册成功
-(void) onApjSDKInitFail:(int) code msg:(NSString*) msg; //初始化失败
-(void) onApjSDKLoginSuccess:(APJUserModel*) userModel; //登陆成功
-(void) onApjSDKLoginFail:(int) code msg:(NSString*) msg; //登陆失败
-(void) onApjSDKLogoutSuccess; //退出登陆
-(void) onApjSDKAccountDeleteSuccess; //删除账号
-(void) onApjBindAPJAccountSuccess:(NSString*)email; //绑定apj账号成功

//payment
-(void) onApjSDKPaymentSuccess:(NSString*) productId
                    apjOrderId:(NSString*) apjOrderId
                     cpOrderId:(NSString*) cpOrderId;       //购买成功
-(void) onApjSDKPaymentFail:(NSInteger) code msg:(NSString*) msg; //购买失败
-(void) onApjSDKQueryProductSuccess:(NSArray<APJProductModel*>*) productDetailsList; //查询商品信息成功
-(void) onApjSDKQueryProductFail:(int) code msg:(NSString*) msg; //查询商品信息失败

-(void) onApjSDKReceiveNotifyToken:(NSString*)token;  //获取谷歌 FCM token
-(void) onApjSDKFetchAppsflyerDeepLinkValueSuccess:(NSString*)deepLinkValue;  //获取af deep_link_value
-(void) onApjSDKShareResult:(APJShareResult)shareResult;  //分享结果回调

-(void) onApjSDKSyncRoleInfoResult:(Boolean)isSuccess msg:(NSString*)msg; //SyncRoleInfo结果

@optional
-(void) onApjReceiveIDFAString:(NSString*)idfaString;  //获取IDFA 可选
-(void) onApjSDKUserType:(int)userType msg:(NSString*)msg; //CR账号非会员：0, 普通会员：1, 付费会员：2, 报错：-1

-(void) onApjSDKGoogleAdClicked:(NSString*) adUnitId;  //谷歌广告：点击广告
-(void) onApjSDKGoogleAdFinishSuccess:(NSString*) adUnitId; //谷歌广告：广告关闭

@end

NS_ASSUME_NONNULL_END
