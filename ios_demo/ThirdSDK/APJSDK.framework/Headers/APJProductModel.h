//
//  APJProductModel.h
//  APJSDK
//
//  Created by Andy on 2023/5/9.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface APJProductModel : NSObject

@property(nonatomic, copy)NSString* productId;           //com.xxx.item1
@property(nonatomic, copy)NSString* name;                //item240
@property(nonatomic, copy)NSString* descriptionString;   //50 diamonds
@property(nonatomic, assign)double priceAmountMicros;    //330
@property(nonatomic, copy)NSString* priceCurrencyCode;   //JPY
@property(nonatomic, copy)NSString* formattedPrice;      //￥300
 
-(instancetype)initWithSK:(SKProduct*)sk;

@end

NS_ASSUME_NONNULL_END
