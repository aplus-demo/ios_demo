//
//  APJLanguageKey.h
//  APJSDK
//
//  Created by Andy on 2023/4/21.
//
#import <Foundation/Foundation.h>

typedef enum : NSInteger {
    EN = 0,
    DE = 1,
    FR = 2,
    PT = 3,
    ES = 4,
    Ar = 5,
    JP = 6,
    KR = 7,
    CN = 8,
    TW = 9,
} APJLanguageType;

//value与language.json里对应
static NSString* apj_hint_license = @"hint_license";
static NSString* apj_hint_policy = @"hint_policy";
static NSString* apj_protocol_title = @"protocol_title";
static NSString* apj_license_text_all = @"license_text_all";
static NSString* apj_license_text_color = @"license_text_color";
static NSString* apj_policy_text_all = @"policy_text_all";
static NSString* apj_policy_text_color = @"policy_text_color";

static NSString* apj_please_input_email = @"please_input_email";
static NSString* apj_please_input_password = @"please_input_password";
static NSString* apj_register_account = @"register_account";
static NSString* apj_find_password = @"find_password";
static NSString* apj_sign_in = @"sign_in";
static NSString* apj_other = @"other";
static NSString* apj_had_send_code = @"had_send_code";
static NSString* apj_please_input_verify_code = @"please_input_verify_code";
static NSString* apj_send_verify_code = @"send_verify_code";
static NSString* apj_please_set_password = @"please_set_password";
static NSString* apj_please_set_password_again = @"please_set_password_again";
static NSString* apj_register_success = @"register_success";
static NSString* apj_please_input_new_password = @"please_input_new_password";
static NSString* apj_please_input_new_password_again = @"please_input_new_password_again";
static NSString* apj_reset = @"reset";
static NSString* apj_please_complete_verify = @"please_complete_verify";
static NSString* apj_current_account = @"current_account";
static NSString* apj_confirm_login = @"confirm_login";
static NSString* apj_link_apj = @"link_apj";
static NSString* apj_sign_out = @"sign_out";
static NSString* apj_link_account = @"link_account";
static NSString* apj_delete_account = @"delete_account";
static NSString* apj_delete_account_content = @"delete_account_content";
static NSString* apj_second_apply_content = @"second_apply_content";
static NSString* apj_cancel = @"cancel";
static NSString* apj_confirm = @"confirm";
static NSString* apj_change_email = @"change_email";
static NSString* apj_change_password = @"change_password";
static NSString* apj_please_input_new_email = @"please_input_new_email";
static NSString* apj_confirm_change = @"confirm_change";
static NSString* apj_account_suspend = @"account_suspend";
static NSString* apj_account_had_suspended_content = @"account_had_suspended_content";
static NSString* apj_operate_success = @"operate_success";
static NSString* apj_reset_password_success = @"reset_password_success";
static NSString* apj_guest_account = @"guest_account";
static NSString* apj_other_account = @"other_account";
static NSString* apj_unregister_url_not_set = @"unregister_url_not_set";
static NSString* apj_note = @"note";
static NSString* apj_account_need_sign_in_again = @"account_need_sign_in_again";

//code
static int apj_error_email_3111 = 3111;
static int apj_error_password_3112 = 3112;
static int apj_error_password_not_same_3114 = 3114;
static int apj_error_verify_code_3113 = 3113;
static int apj_error_login_fail_3101 = 3101;
