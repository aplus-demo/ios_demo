//
//  APJDeviceUtil.h
//  APJSDK
//
//  Created by Andy on 2023/4/21.
//

#import <Foundation/Foundation.h>
#import <APJSDK/APJLanguageKey.h>

NS_ASSUME_NONNULL_BEGIN

static NSString* APJSDKVersion = @"3.5.6";

//线程
#define MainThread(mainBlock) dispatch_async(dispatch_get_main_queue(), mainBlock);
#define SubThread(subBlock) dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), subBlock);
#define MainDelay(time, mainBlock)dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(time * NSEC_PER_SEC)), dispatch_get_main_queue(), mainBlock);

//颜色定义
#define APJColor(rgbValue) \
    [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16)) / 255.0 \
                    green:((float)((rgbValue & 0xFF00) >> 8)) / 255.0 \
                    blue :((float)(rgbValue & 0xFF)) / 255.0 alpha:1.0]
#define APJColorBlack52 APJColor(0x525252)
#define APJColorGrayLight APJColor(0xF4F4F4)
#define APJColorGrayDark APJColor(0xBEBEBE)
#define APJColorBeige APJColor(0xF5D9D0)
#define APJColorCheese APJColor(0xF19E2E)
#define APJColorGreen APJColor(0x1DC53C)
#define APJColorOrange APJColor(0xF76E40)
#define APJColorGrayA4 APJColor(0xa4a4a4)
#define APJColorPlaceholder APJColor(0xB2B2B2)

@interface APJDeviceUtil : NSObject

+(void)print:(NSString*) msg;

+(void)print:(NSString*) title msg:(NSString*)msg;

+(NSString*)getDID;

+(NSString*)getPID;

+(NSString*)getDeviceBrand;

+(NSString*)getDeviceModel;

+(NSString*)getDeviceOsVersion;

+(NSString*)getReferrerMedia;

+(Boolean)isBreakDevice; //是否越狱

+(NSString*)getBundleId;

@end

NS_ASSUME_NONNULL_END
