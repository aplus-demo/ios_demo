//
//  APJUnityService.h
//  APJSDK
//
//  Created by Andy on 2023/5/24.
//

#import <Foundation/Foundation.h>


NS_ASSUME_NONNULL_BEGIN

@interface APJUnityService : NSObject

+(void)initWithSDKLanguage:(NSString*)initWithSDKLanguage
            bridgeCallback:(void (^)(NSString *result))callback;

+(void)setSDKLanguage:(NSString*)setSDKLanguage;

+(void)startLogin;

+(void)openAccountCenter;

+(NSDictionary*)getCurrentUser; //返回user json

+(void)gAccountId:(NSString*)gAccountId
        gServerId:(NSString*)gServerId
      gServerName:(NSString*)gServerName
          gRoleId:(NSString*)gRoleId
        gRoleName:(NSString*)gRoleName
           gLevel:(NSString*)gLevel;

+(void)productId:(NSString*)productId
     cpAccountId:(NSString*)cpAccountId
      cpServerId:(NSString*)cpServerId
        cpRoleId:(NSString*)cpRoleId
       cpOrderId:(NSString*)cpOrderId
     cpNotifyUrl:(NSString*)cpNotifyUrl
     cpRefundUrl:(NSString*)cpRefundUrl
       cpExtInfo:(NSString*)cpExtInfo;

+(void)queryWithProductIds:(NSArray<NSString*>*)queryWithProductIds;

+(void)track:(NSString*)track
        name:(NSString*)name
        json:(NSString*)json;

+(void)fetchAppsflyerDeepLinkValue;

+(void)shareFacebookPathOrUrl:(NSString*)shareFacebookPathOrUrl;

+(void)shareFacebookImageList:(NSArray<NSString*>*)shareFacebookImageList;

+(void)shareNativeList:(NSArray<NSString*>*)shareNativeList;

+(void)openAIHelp:(NSString*)openAIHelp;

+(NSDictionary*)getSDKVersion;

+(void)requestReview;

+(void)checkUserVIPType; //非会员：0, 普通会员：1, 付费会员：2, 报错：-1

+(NSDictionary*)getDeviceId;

+(NSDictionary*)getInstallId; //数数的 ta_distinct_id 也是用的这个值

+(void)openWebUrl:(NSString*)openWebUrl;

+(void)logout;

+(NSDictionary*)checkSingularDeeplink;

+(void)requestIDFAPermission;

+(void)showGoogleInterstitialAd:(NSString*)showGoogleInterstitialAd;

@end

NS_ASSUME_NONNULL_END
