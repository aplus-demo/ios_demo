//
//  APJSDK.h
//  APJSDK
//
//  Created by Andy on 2023/4/21.
//

#import <Foundation/Foundation.h>
#import <APJSDK/APJLanguageKey.h>
#import <APJSDK/APJSDKCallback.h>
#import <APJSDK/APJUserModel.h>
#import <APJSDK/APJDeviceUtil.h>
#import <APJSDK/APJUnityService.h>
#import <APJSDK/APJProductModel.h>

NS_ASSUME_NONNULL_BEGIN

@interface APJSDK : NSObject

+(void)initSDK:(APJLanguageType)languageType
sdkCallbackDelegate:(id<APJSDKCallback>)callback;

+(void)setSDKLanguage:(APJLanguageType)languageType;

+(BOOL)isInitialized;

+(BOOL)isLogin;

+(void)startLogin;

+(void)openAccountCenter;

+(APJUserModel*)getCurrentUser;

//重要，每个参数都是非空、非length=0的字符串！
+(void)syncRoleInfo:(NSString*)gAccountId
          gServerId:(NSString*)gServerId
        gServerName:(NSString*)gServerName
            gRoleId:(NSString*)gRoleId  //数数的 ta_account_id 也是这个值！如果服务器有用数数api请保存一致。
          gRoleName:(NSString*)gRoleName
             gLevel:(NSString*)gLevel;

//V3版本
+(void)buyProduct:(NSString*)productId
      cpAccountId:(NSString*)cpAccountId
       cpServerId:(NSString*)cpServerId
         cpRoleId:(NSString*)cpRoleId
        cpOrderId:(NSString*)cpOrderId   //每次都是唯一值
      cpNotifyUrl:(NSString*)cpNotifyUrl //给游戏服务器 购买成功通知url
      cpRefundUrl:(NSString*)cpRefundUrl //给游戏服务器 退款通知url
        cpExtInfo:(NSString*)cpExtInfo;

+(void)queryProductInfo:(NSArray<NSString*>*)productIdList;

+(void)trackEvent:(APJEventPlatform)platform eventName:(NSString*)eventName valueMap:(nullable NSDictionary*)dic;

+(void)fetchAppsflyerDeepLinkValue;

+(void)shareFacebookPathOrUrl:(NSString*)pathOrUrl;

+(void)shareFacebookImageList:(NSArray<NSString*>*)pathOrUrlList;

+(void)shareNativeList:(NSArray<NSString*>*)pathOrUrlList;

+(void)didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken;

+(void)openAIHelp:(NSString*)welcomeMsg;

+(NSString*)getSDKVersion;

+(void)requestReview;

+(void)checkUserVIPType; //非会员：0, 普通会员：1, cr付费会员：2, apj会员：3，报错：-1

+(NSString*)getDeviceId;

+(NSString*)getInstallId; //数数的 ta_distinct_id 也是用的这个值

+(void)openWebUrl:(NSString*)webUrl;

+(void)updateDictionary:(NSDictionary *)launchOptions;

+(void)updateActivity:(NSUserActivity *)userActivity;

+(void)updateOpenUrl:(NSURL *)url;

+(void)logout;

+(NSString*)checkSingularDeeplink;

+(void)requestIDFAPermission;

//展示谷歌插屏广告
+(void)showGoogleInterstitialAd:(NSString*)adUnitId;

@end


NS_ASSUME_NONNULL_END
