#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "TAAdjustSyncData.h"
#import "TAAppLovinSyncData.h"
#import "TAAppsFlyerSyncData.h"
#import "TABaseSyncData.h"
#import "TABranchSyncData.h"
#import "TAFirebaseSyncData.h"
#import "TAIronSourceSyncData.h"
#import "TAKochavaSyncData.h"
#import "TAReYunSyncData.h"
#import "TAThirdParty.h"
#import "TAThirdPartyManager.h"
#import "TAThirdPartySyncProtocol.h"
#import "TATopOnSyncData.h"
#import "TATradPlusSyncData.h"

FOUNDATION_EXPORT double TAThirdPartyVersionNumber;
FOUNDATION_EXPORT const unsigned char TAThirdPartyVersionString[];

