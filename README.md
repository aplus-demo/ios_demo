# APJSDK


#### 1.接口都在APJSDK.h文件中，回调都在APJSDKCallback.h文件中 

![01](https://gitlab.com/aplus-demo/ios_demo/-/raw/main/Resource/001.jpeg)
![02](https://gitlab.com/aplus-demo/ios_demo/-/raw/main/Resource/002.jpeg)
![03](https://gitlab.com/aplus-demo/ios_demo/-/raw/main/Resource/003.jpeg)
![04](https://gitlab.com/aplus-demo/ios_demo/-/raw/main/Resource/004.jpeg)
![05](https://gitlab.com/aplus-demo/ios_demo/-/raw/main/Resource/005.jpeg)
![06](https://gitlab.com/aplus-demo/ios_demo/-/raw/main/Resource/006.jpeg)
![07](https://gitlab.com/aplus-demo/ios_demo/-/raw/main/Resource/007.jpeg)
![08](https://gitlab.com/aplus-demo/ios_demo/-/raw/main/Resource/008.jpeg)

[如果运行报错，可以这样处理](https://forums.developer.apple.com/forums/thread/730974)
![12](https://gitlab.com/aplus-demo/ios_demo/-/raw/main/Resource/010.jpeg)

1. 下载 [ThirdSDK.zip](https://gitlab.com/aplus-demo/ios_demo/-/blob/main/Resource/ThirdSDK.zip) 解压后放入项目文件夹中，添加里面所有的 framework 和 bundle 到项目中，framework配置都是 `Do Not Embed`。
2. [下载配置文件模板](https://gitlab.com/aplus-demo/ios_demo/-/blob/main/Resource/apj_sdk_config.json) 里面的xxx参数我们会提供文档，如果没有对应参数，可以把对应配置删除。
3. 将`GoogleService-Info.plist`文件并添加到项目中，我们会提供这个文件，然后在 `项目的 info.plist`中的 `URL types`里添加谷歌 `URL Schemes`， 值是`GoogleService-Info.plist`文件里对应`REVERSED_CLIENT_ID`值。
4. 在 `AppDelegate.m` 中添加如下代码。

```
1.
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    ....
    [APJSDK updateDictionary:launchOptions];
    return YES;
}

2.
- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options{
   ....  
    [APJSDK updateOpenUrl:url];
    return YES;
}

3.
- (BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void (^)(NSArray<id<UIUserActivityRestoring>> * _Nullable))restorationHandler{
   ....
    [APJSDK updateActivity:userActivity];
    return YES;
}

4.
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
    [APJSDK didRegisterForRemoteNotificationsWithDeviceToken:deviceToken];
}

```
5. 在Run Script 中添加如下配置,提交dSYM文件到Firebase用于崩溃分析。 [参考链接](https://firebase.google.com/docs/crashlytics/get-deobfuscated-reports?hl=zh-cn&platform=ios)  [参考2](https://firebase.google.com/docs/crashlytics/get-started?hl=zh-cn&platform=ios)

![08](https://gitlab.com/aplus-demo/ios_demo/-/raw/main/Resource/011.png)
```
//注意是定位到ThridSDK里的run文件
"${PROJECT_DIR}/../ThirdSDK/run"

//Unity生成的Xcode是如下路径, 即`ThirdSDK/run`文件相对于 `.xcodeproj`文件的路径。
"${PROJECT_DIR}/ThirdSDK/run"

//Input Files 配置
${DWARF_DSYM_FOLDER_PATH}/${DWARF_DSYM_FILE_NAME}/Contents/Resources/DWARF/${TARGET_NAME}
$(SRCROOT)/$(BUILT_PRODUCTS_DIR)/$(INFOPLIST_PATH)
```
6. 在项目的 `info.plist` 中添加如下 key-value 固定值， appsflyer用的.如果没配置appsflyer key的话可以不加。
```
key:  NSAdvertisingAttributionReportEndpoint 
value:  https://appsflyer-skadnetwork.com
```

7. Xcode 需要添加 `PrivacyInfo`文件，苹果审核新规要求！[参考链接1,](https://cloud.tencent.com/document/product/269/104138?from_column=20421&from=20421) [参考链接2,](https://cloud.tencent.com/developer/article/2397278) 

#### APJSDK API 概览，对外接口都在APJSDK.h文件中
```

@interface APJSDK : NSObject

//初始化和绑定回调
+(void)initSDK:(APJLanguageType)languageType
sdkCallbackDelegate:(id<APJSDKCallback>)callback;

//设置语言
+(void)setSDKLanguage:(APJLanguageType)languageType;

//打开登录弹框，如果已经登录过会自动登录
+(void)startLogin; 

//打开账号中心
+(void)openAccountCenter; 

//【重要】，游戏登录成功后尽快调用，参数都不能为空或者空字符！！！。
+(void)syncRoleInfo:(NSString*)gAccountId
          gServerId:(NSString*)gServerId
        gServerName:(NSString*)gServerName
            gRoleId:(NSString*)gRoleId  //数数的 ta_account_id 也是这个值！如果服务器有用数数api请保存一致。
          gRoleName:(NSString*)gRoleName
             gLevel:(NSString*)gLevel;

//支付
+(void)buyProduct:(NSString*)productId
      cpAccountId:(NSString*)cpAccountId
       cpServerId:(NSString*)cpServerId
         cpRoleId:(NSString*)cpRoleId
        cpOrderId:(NSString*)cpOrderId   //每次都是唯一值
      cpNotifyUrl:(NSString*)cpNotifyUrl //给游戏服务器 购买成功通知url
      cpRefundUrl:(NSString*)cpRefundUrl //给游戏服务器 退款通知url
        cpExtInfo:(NSString*)cpExtInfo;

//查询商品信息，如货币单位，金额
+(void)queryProductInfo:(NSArray<NSString*>*)productIdList;

//埋点
+(void)trackEvent:(APJEventPlatform)platform eventName:(NSString*)eventName valueMap:(nullable NSDictionary*)dic;

//Facebook分享
+(void)shareFacebookPathOrUrl:(NSString*)pathOrUrl;

+(void)shareFacebookImageList:(NSArray<NSString*>*)pathOrUrlList;

//本地分享
+(void)shareNativeList:(NSArray<NSString*>*)pathOrUrlList;

+(void)didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken;

+(void)openAIHelp:(NSString*)welcomeMsg;

+(NSString*)getSDKVersion;

+(void)requestReview; //打开appStroe评分弹框

+(void)checkUserVIPType; 
报错: -1   报错信息
非会员: 0  is_third_account
APJ账号: 1  apj_account
CR账号: 2  cr_account
CR付费会员: 3  cr_fan
CR付费会员mega: 4  cr_mega_fan
CR付费会员ultimate: 5   cr_ultimate_fan


//获取设备ID, 卸载重装不变
+(NSString*)getDeviceId; 

//【重要】获取安装ID, 卸载重装会变。【要吧这个值给游戏服务器端，用于服务器端的数数游客ID】
+(NSString*)getInstallId; 

+(void)openWebUrl:(NSString*)webUrl; //打开网页url

+(void)singularUpdateDictionary:(NSDictionary *)launchOptions;//没配置singular参数可以不用加这方法

+(void)singularUpdateActivity:(NSUserActivity *)userActivity;//没配置singular参数可以不用加这方法

+(void)singularUpdateOpenUrl:(NSURL *)url;//没配置singular参数可以不用加这方法

+(NSString*)getSDKVersion;

+(void)logout;
@end

```

#### APJSDK 回调概览，对外回调都在 APJSDKCallback.h 文件中
```
@protocol APJSDKCallback <NSObject>

//account
-(void) onApjSDKInitSuccess; //初始化成功
-(void) onApjSDKInitFail:(int) code msg:(NSString*) msg; //初始化失败
-(void) onApjEmailRegisterSuccess:(NSString*) email; //APJ Email 注册成功
-(void) onApjSDKLoginSuccess:(APJUserModel*) userModel; //登陆成功
-(void) onApjSDKLoginFail:(int) code msg:(NSString*) msg; //登陆失败
-(void) onApjSDKLogoutSuccess; //退出登陆
-(void) onApjSDKAccountDeleteSuccess; //删除账号
-(void) onApjBindAPJAccountSuccess:(NSString*)email; //绑定apj账号成功

//payment
-(void) onApjSDKPaymentSuccess:(NSString*) productId
                    apjOrderId:(NSString*) apjOrderId
                     cpOrderId:(NSString*) cpOrderId;       //购买成功
-(void) onApjSDKPaymentFail:(NSInteger) code msg:(NSString*) msg; //购买失败
-(void) onApjSDKQueryProductSuccess:(NSArray<APJProductModel*>*) productDetailsList; //查询商品信息成功
-(void) onApjSDKQueryProductFail:(int) code msg:(NSString*) msg; //查询商品信息失败

-(void) onApjSDKReceiveNotifyToken:(NSString*)token;  //获取谷歌 FCM token
-(void) onApjSDKFetchAppsflyerDeepLinkValueSuccess:(NSString*)deepLinkValue;  //获取af deep_link_value
-(void) onApjSDKShareResult:(APJShareResult)shareResult;  //分享结果回调

@optional
-(void) onApjReceiveIDFAString:(NSString*)idfaString;  //获取IDFA 可选
-(void) onApjSDKGoogleAdClicked:(NSString*) adUnitId;  //谷歌广告：点击广告
-(void) onApjSDKGoogleAdFinishSuccess:(NSString*) adUnitId; //谷歌广告：广告关闭

报错: -1   报错信息
非会员: 0  is_third_account
APJ账号: 1  apj_account
CR账号: 2  cr_account
CR付费会员: 3  cr_fan
CR付费会员mega: 4  cr_mega_fan
CR付费会员ultimate: 5   cr_ultimate_fan
-(void) onApjSDKUserType:(int)userType msg:(NSString*)msg; 


@end

```

### 接口使用，引入 #import <APJSDK/APJSDK.h>

#### 初始化SDK,初始化成功后才可以使用其他接口
```

[APJSDK initSDK:APJLanguageType sdkCallbackDelegate:APJSDKCallback];

-(void) onApjSDKInitSuccess;                             //初始化成功
-(void) onApjSDKInitFail:(int) code msg:(NSString*) msg; //初始化失败 

1.SDK支持语言类型：
typedef enum : NSInteger {
    EN = 0,  英语
    DE = 1,  德语
    FR = 2,  法语
    PT = 3,  葡萄牙语
    ES = 4,  西班牙语
    Ar = 5,  阿拉伯语
    JP = 6,  日语
    KR = 7,  韩语
    CN = 8,  简体中文
    TW = 9,  繁体中文
} APJLanguageType;

```

#### 初始化成功后 弹出SDK登录弹框
```
[APJSDK startLogin];

回调接口：
-(void) onApjSDKLoginSuccess:(APJUserModel*) userModel;   //登陆成功
-(void) onApjSDKLoginFail:(int) code msg:(NSString*) msg; //登陆失败
```

#### [重要]同步游戏信息给SDK，登录成功后调用，都是string 不能空或者空字符串！
```
[APJSDK syncRoleInfo:gAccountId  如果没有就和roleId一样
           gServerId:gServerId   服务器ID
         gServerName:gServerName 服务器名称 
             gRoleId:gRoleId     角色ID。数数account_id也是这个，游戏服务端也要用这个
           gRoleName:gRoleName   角色昵称
              gLevel:gRoleLevel];等级
```

#### 设置SDK语言
```
[APJSDK setSDKLanguage:APJLanguageType];

SDK支持语言类型：
typedef enum : NSInteger {
    EN = 0,  英语
    DE = 1,  德语
    FR = 2,  法语
    PT = 3,  葡萄牙语
    ES = 4,  西班牙语
    Ar = 5,  阿拉伯语
    JP = 6,  日语
    KR = 7,  韩语
    CN = 8,  简体中文
    TW = 9,  繁体中文
} APJLanguageType;
```

#### 打开账号中心
```
[APJSDK openAccountCenter];
```

#### 获取苹果商品信息列表
```
注意！！
1.查询数量和返回数量可能会不一样的,如果这个商品不存在的话就不会返回这一项。如果商品ID重复也只会返回一个!
2.商品列表显示的货币单位和金额正常是和支付弹框显示的单位和金额一样的, 如果不一样就是商品列表显示有问题！
3.不要有空格或者换行符！注意返回结果和参数不是一一对应的，返回结果数量 <= 查询参数数量！

NSMutableArray* ary = [[NSMutableArray alloc] init];
[ary addObject:@"苹果后台的商品ID"];
[ary addObject:@"苹果后台的商品ID"];
[APJSDK queryProductInfo:ary];

回调接口：
-(void) onApjSDKQueryProductSuccess:(NSArray<APJProductModel*>*) productDetailsList; //查询商品信息成功
-(void) onApjSDKQueryProductFail:(int) code msg:(NSString*) msg;                     //查询商品信息失败

// APJProductModel 信息如下
@property(nonatomic, copy)NSString* productId;           //com.xxx.item1
@property(nonatomic, copy)NSString* name;                //item240
@property(nonatomic, copy)NSString* descriptionString;   //50diamonds
@property(nonatomic, assign)double priceAmountMicros;    //330
@property(nonatomic, copy)NSString* priceCurrencyCode;   //JPY
@property(nonatomic, copy)NSString* formattedPrice;      //￥300

```

#### 购买苹果商品
```
[APJSDK buyProduct:productId   苹果后台的商品ID
       cpAccountId:cpAccountId 游戏账号ID
        cpServerId:cpServerId  游戏服务ID
          cpRoleId:cpRoleId    角色ID
         cpOrderId:cpOrderId   游戏的订单ID,唯一字符串
       cpNotifyUrl:cpNotifyUrl SDK服务器通知游戏服务器购买结果的url 
       cpRefundUrl:(NSString*)cpRefundUrl //给游戏服务器 退款通知url
         cpExtInfo:cpExtInfo];   透传信息，会在notify url里给游戏 
       
回调接口：
-(void) onApjSDKPaymentSuccess:(NSString*) productId
                    apjOrderId:(NSString*) apjOrderId
                     cpOrderId:(NSString*) cpOrderId;             //购买成功
-(void) onApjSDKPaymentF ail:(NSInteger) code msg:(NSString*) msg; //购买失败


【要注意的 code：】
4002： 用户取消购买
-1000： 网络失败
5201： 每月购买额度被限制了，日本地区才有

苹果购买失败code参考：
4000	SKErrorUnknown
4001	SKErrorClientInvalid
4002	SKErrorPaymentCancelled 【用户取消购买】
4003	SKErrorPaymentInvalid
4004	SKErrorPaymentNotAllowed
4005	SKErrorStoreProductNotAvailable
4006	SKErrorCloudServicePermissionDenied
4007	SKErrorCloudServiceNetworkConnectionFailed
4008	SKErrorCloudServiceRevoked
4009	SKErrorPrivacyAcknowledgementRequired
4010	SKErrorUnauthorizedRequestData
4011	SKErrorInvalidOfferIdentifier
4012	SKErrorInvalidOfferPrice
4013	SKErrorInvalidSignature
4014	SKErrorMissingOfferParams
4015	SKErrorIneligibleForOffer
4016	SKErrorOverlayCancelled
4017	SKErrorOverlayInvalidConfiguration
4018	SKErrorOverlayPresentedInBackgroundScene
4019	SKErrorOverlayTimeout
4020	SKErrorUnsupportedPlatform
5101    Payment is processing
5102    Device not support payment
5103    Payment has purchase to upload
5104    Not found product
5105    APJ order id is empty
5201    今月の課金額はすでに上限を達していますため、制限がかけられています。来月からもう一度試みてください。
-1000   Network Error 【网络失败】
```

#### 事件埋点，内部有Facebook,Appsflyer,Firebase埋点 
```
[APJSDK trackEvent:platform //埋点平台
                eventName:eventName 事件名称 
                  valueMap:@{
                            @"key":@"value",  key 字符串
                            @"key":@"value",  value 字符串  
                           }];
//支持这些平台
typedef enum : NSUInteger {
    AppsflyerEvent,
    SingularEvent,
    SegmentEvent,
    ThinkingDataEvent,
    FirebaseEvent,
} APJEventPlatform;

```

#### Facebook分享，
```
1. 
[APJSDK shareFacebookPathOrUrl:pathOrUrl]; 分享单个图片链接或者图片路径

2. 
NSMutableArray* ary = [[NSMutableArray alloc] init]; 链接或路径
[ary addObject:@"链接/图片路径"];
[ary addObject:@"链接/图片路径"];
[APJSDK shareFacebookImageList:pathOrUrl]; 分享多个图片链接或者图片路径

回调接口：
-(void) onApjSDKShareResult:(APJShareResult)shareResult;  //分享结果回调      //分享结果回调

typedef enum : NSUInteger {
    ShareSuccess = 0,
    ShareCancel = 1,
    ShareFail = 2,
    NativeShareFinish = 3,
} APJShareResult;     
```

#### App原生分享
```
NSMutableArray* ary = [[NSMutableArray alloc] init]; 链接或路径
[ary addObject:@"链接/图片路径"]; 
[ary addObject:@"链接/图片路径"];
[APJSDK shareNativeList:ary];      
```

#### 打开客服平台
```
APJ: [APJSDK openAIHelp:welcomeMsg];  
CR : [APJSDK openWebUrl:@"https://games.help.crunchyroll.com/hc/en-us"];
```

#### 检查用户类型
```
[APJSDK checkUserVIPType];

结果在回调里获取：
报错: -1   报错信息
非会员: 0  is_third_account
APJ账号: 1  apj_account
CR账号: 2  cr_account
CR付费会员: 3  cr_fan
CR付费会员mega: 4  cr_mega_fan
CR付费会员ultimate: 5   cr_ultimate_fan
-(void) onApjSDKUserType:(int)userType msg:(NSString*)msg;

```

#### 获取Singular deep link
```
NSString* link = [APJSDK checkSingularDeeplink];
可能空，可能https:/xxx   可能xxxx://xxxx
```

#### 退出登录
```
[APJSDK logout];

回调：onApjSDKLogoutSuccess
```


#### 打开谷歌广告
```
1.需要在plist里添加谷歌配置, cstr6suwn9.skadnetwork 是固定值。

	<key>SKAdNetworkItems</key>
	<array>
		<dict>
			<key>SKAdNetworkIdentifier</key>
			<string>cstr6suwn9.skadnetwork</string>
		</dict>
	</array>
	<key>GADApplicationIdentifier</key>
	<string>这里是谷歌广告后台的APPID</string>


2.调用打开谷歌广告方法
[APJSDK showGoogleInterstitialAd:谷歌广告位ID];

3. 广告回调
-(void) onApjSDKGoogleAdClicked:(NSString*) adUnitId;  //点击广告
-(void) onApjSDKGoogleAdFinishSuccess:(NSString*) adUnitId; // 广告看完关闭
```







